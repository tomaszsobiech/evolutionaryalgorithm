import random
import control_system
import math

class Individual:
    def __init__(self, values_range_dict, fitness_function):
        # chromosomes store variable (x, sigma) - x - value, sigma - variance of x value.
        # sigma - used for mutation
        self.chromosomes = []
        for values_range in values_range_dict.values():
            initial_chromosome_value = random.uniform(values_range['lower_range'], values_range['higher_range'])
            initial_sigma = 1
            self.chromosomes.append((initial_chromosome_value, initial_sigma))
        self.fitness = self.fitness_calculation(fitness_function)


    def fitness_calculation(self, callback_funcion):
        return callback_funcion(self.chromosomes)

class Population:
    def __init__(self, population_size, values_range_dict, crossover_probability, mutation_probability, fitness_callback_function):
        self.fitness_callback = fitness_callback_function
        self.crossover_probability = crossover_probability
        self.mutation_probability = mutation_probability
        self.population_size = population_size
        self.values_range_dict = values_range_dict
        self.individual_list = []

        for i in range(self.population_size):
            individual = Individual(values_range_dict, fitness_callback_function)
            self.individual_list.append(individual)

    def generation(self):
        """

        :return:
        """
        self.mutation()
        self.crossover()
        self.mutation()
        #self.selection_roulette_wheel()
        self.selection_rank()

        return self.fitness_evaluation()

    def crossover(self):
        """

        :return:
        """
        random.shuffle(self.individual_list)

        for index_individual in range(0, len(self.individual_list) - 1, 2):
            # do crossover on this pair of individuals?
            do_crossover = random.random()
            if do_crossover < self.crossover_probability:
                # using avg
                chromosomes = []

                for chromosome_index in range(len(self.individual_list[0].chromosomes)):
                    try:
                        avg_chromosome_parameter = (self.individual_list[index_individual].chromosomes[chromosome_index][0] + self.individual_list[index_individual+1].chromosomes[chromosome_index][0])/2
                    except:
                        avg_chromosome_parameter = 0
                    try:
                        avg_chromosome_sigma = (self.individual_list[index_individual].chromosomes[chromosome_index][1] + self.individual_list[index_individual+1].chromosomes[chromosome_index][1])/2
                    except:
                        avg_chromosome_sigma = 0
                    chromosomes.append((avg_chromosome_parameter, avg_chromosome_sigma))

                # creating new Individual and adding into Population
                new_Individual = Individual(self.values_range_dict, self.fitness_callback)
                new_Individual.chromosomes = chromosomes
                self.individual_list.append(new_Individual)

    def mutation(self):
        """

        :return:
        """
        for index, individual in enumerate(self.individual_list):
            do_mutation = random.random()
            if do_mutation < self.mutation_probability:
                chromosome_for_mutation = random.randint(0, len(individual.chromosomes)-1)

                values_list = []
                for value in self.values_range_dict.values():

                    values_list.append(value)

                gaussian_distribution = random.gauss(0, individual.chromosomes[chromosome_for_mutation][1])
                mutated_parameter = individual.chromosomes[chromosome_for_mutation][0] + gaussian_distribution
                mutated_sigma = individual.chromosomes[chromosome_for_mutation][1] + gaussian_distribution

                if mutated_parameter > values_list[chromosome_for_mutation]['higher_range']:
                    #mutated_parameter = values_list[chromosome_for_mutation]['higher_range']
                    mutated_parameter -= 0.1*random.uniform(values_list[chromosome_for_mutation]['lower_range'], values_list[chromosome_for_mutation]['higher_range'])
                elif mutated_parameter < values_list[chromosome_for_mutation]['lower_range']:
                    mutated_parameter += 0.1*random.uniform(values_list[chromosome_for_mutation]['lower_range'], values_list[chromosome_for_mutation]['higher_range'])
                    #mutated_parameter = mutated_parameter < values_list[chromosome_for_mutation]['lower_range']

                self.individual_list[index].chromosomes[chromosome_for_mutation] = (mutated_parameter, mutated_sigma)
                a = individual.chromosomes
                self.individual_list[index].fitness = individual.fitness_calculation(self.fitness_callback)

    def selection_roulette_wheel(self):
        """
        Selekcja metodą ruletki.
        :return:
        """
        new_population = []

        sum_fitness_population = self._sum_fitness_of_population()
        parts_of_wheel = []
        part_of_wheel = 0

        for individual in self.individual_list:
            part_of_wheel += (individual.fitness / sum_fitness_population)

            parts_of_wheel.append(part_of_wheel)
        parts_of_wheel[-1] = 1  # deleting the round error (like 0.9999999999999999999999999 != 1).

        # Add the best Individual into new Population
        for i in range(self.population_size):
            spin_the_wheel = random.random()

            for index, part in enumerate(parts_of_wheel):
                if part > spin_the_wheel:
                    new_population.append(self.individual_list[index])
                    break
        self.individual_list = new_population

    def selection_rank(self):
        new_population = []

        self.individual_list.sort(key=lambda x: getattr(x, "fitness"))

        self.individual_list.reverse()

        for i in range(round(self.population_size/25)+1):
            for j in range(round(self.population_size/25)+1 - i):
                new_population.append(self.individual_list[i])

        for i in range(self.population_size):
            new_population.append(self.individual_list[i])

        random.shuffle(new_population)

        new_population = new_population[:self.population_size]
        self.individual_list = new_population
        #print('new pop size = ', len(new_population))
        return self.individual_list

    def fitness_evaluation(self):
        """
        Search best Individual in Population
        :return: Best Individual
        """
        best_individual = self.individual_list[0]
        for individual in self.individual_list:
            if individual.fitness > best_individual.fitness:
                best_individual = individual
        return best_individual


    def _sum_fitness_of_population(self):
        sum_fitness = 0
        for individual in self.individual_list:
            sum_fitness += individual.fitness
        return sum_fitness

