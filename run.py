import population
import control_system
import matplotlib.pyplot as plt
import numpy as np
import math

# sources:
# AG:
# http://www.jakubw.pl/com/bib/1996_Alg_genetyczne_cwiczenia.pdf page 26.
# Digital PID
# https: // eia.pg.edu.pl / documents / 1111711 / 53561182 / W04_SCR_swiat_analogowy_a_cyfrowy_cd.pdf
show_presentation = input('Show the AG presentation? (y - yes or else - no)\n\r')

number_of_generations = int(input('Enter number of generations <0, inf>  '))
population_size = int(input('Enter population size <0, inf>  '))
crossover_probability = float(input('Enter crossover probability <0, 1>  '))
mutation_probability = float(input('Enter mutation probability <0, 1>  '))

if show_presentation == 'y':
    # easy equation
    values_range_dict = {
        'x': {'lower_range': -25, 'higher_range': 25},
        'y': {'lower_range': -25, 'higher_range': 25},
    }

    def fitness_function(*args):

        fitness = 4 - math.pow(args[0][0][0], 2) - math.pow((args[0][1][0] - 1), 2) + 10000

        return fitness

    Population = population.Population(
        population_size=population_size,  # Population size should be even
        values_range_dict=values_range_dict,
        crossover_probability=crossover_probability,
        mutation_probability=mutation_probability,
        fitness_callback_function=fitness_function
    )

    x, y = [], []
    best_of_best = Population.individual_list[0]
    for i in range(number_of_generations):
        x.append([])
        y.append([])
        print(i, ' generation')
        best_individual = Population.generation()

        for Individual in Population.individual_list:

            if Individual.fitness > best_of_best.fitness:
                best_of_best = Individual

            x[i].append(Individual.chromosomes[0][0])
            y[i].append(Individual.chromosomes[1][0])

    plt.ion()

    decision = True


    while decision is True:
        fig, ax = plt.subplots()
        for index in range(0, number_of_generations, 5):
            x_plot, y_plot = [], []

            sc = ax.scatter(x, y, s=1.2)
            plt.xlim(-25, 25)
            plt.ylim(-25, 25)
            plt.grid()

            x_plot = x[index]
            y_plot = y[index]
            sc.set_offsets(np.c_[x_plot, y_plot])
            plt.xlabel('x')
            plt.ylabel('y')
            plt.title('Population, generation {0}'.format(index))
            fig.canvas.draw_idle()
            plt.pause(0.1)
            plt.cla()
        plt.close()
       # plt.waitforbuttonpress()
        decision = input('Show one more time? (y - yes or any else - no)')
        if decision == y:
            decision = False
        else:
            decision = True

    # Used for gif making
    # # for index in range(0, len(x), 20):
    # for index in range(0, 200, 5):
    #     print(index)
    #     x_plot, y_plot = [], []
    #     fig, ax = plt.subplots()
    #     sc = ax.scatter(x, y, s=1.2)
    #     plt.xlim(-25, 25)
    #     plt.ylim(-25, 25)
    #     plt.grid()
    #     x_plot = x[index]
    #     y_plot = y[index]
    #
    #     sc.set_offsets(np.c_[x_plot, y_plot])
    #     plt.xlabel('x')
    #     plt.ylabel('y')
    #     plt.title('Population, generation {0}'.format(index))
    #
    #     fig.canvas.draw_idle()
    # # plt.pause(0.1)
    # # plt.cla()
    # x_plot, y_plot = [], []
    # fig, ax = plt.subplots()
    # sc = ax.scatter(x, y, s=1.2)
    # plt.xlim(-25, 25)
    # plt.ylim(-25, 25)
    # plt.grid()
    #
    # x_plot = x[index]
    # y_plot = y[index]
    # sc.set_offsets(np.c_[x_plot, y_plot])
    # plt.xlabel('x')
    # plt.ylabel('y')
    # plt.title('Population, generation {0}'.format(200))
    # fig.canvas.draw_idle()
    # plt.waitforbuttonpress()
    # a = 1

else:
    values_range_dict = {
        'Kp': {'lower_range': -3, 'higher_range': 3},
        'Ti': {'lower_range': 0, 'higher_range': 1},
        'Td': {'lower_range': 0, 'higher_range': 1},
    }

    def fitness_function(*args):
        # PID
        time_range = 1000
        x = []
        for i in range(time_range):
            if i < time_range / 5:
                x.append(5)
            elif i < 2 * (time_range / 5):
                x.append(23)
            elif i < 3 * (time_range / 5):
                x.append(-8)
            elif i < 4 * (time_range / 5):
                x.append(14)
            else:
                x.append(-3)

        Ts = 1
        Kp = args[0][0][0]
        Ti = args[0][1][0]
        Td = args[0][2][0]

        PidControlSystem = control_system.pid_control_system(x_list=x, Kp=Kp, Ti=Ti, Td=Td, Ts=Ts)
        ControlObject = control_system.ControlObject(0.1, 0.5)
        y = PidControlSystem.simulate_control_system(time_range=time_range, control_object=ControlObject)

        fitness = 0

        for i in range(time_range):
            fitness += abs(y[i] - x[i])
        if fitness > 9999999999 or fitness == float('Inf') or math.isnan(fitness):
            fitness = 9999999999

        max_fitness = 9999999999
        fitness = -fitness + max_fitness

        return fitness

    time_range = 1000


    Population = population.Population(
        population_size=population_size,  # Population size should be even
        values_range_dict=values_range_dict,
        crossover_probability=crossover_probability,
        mutation_probability=mutation_probability,
        fitness_callback_function=fitness_function
    )
    best_of_best = Population.individual_list[0]
    for i in range(number_of_generations):
        print(i, ' generation')
        best_individual = Population.generation()
        for Individual in Population.individual_list:
            if Individual.fitness > best_of_best.fitness:
                best_of_best = Individual

    # PID
    ########################################################################################################################
    ##########################################---------------TESTING----------------########################################
    ########################################################################################################################


    x = []
    for i in range(time_range):
        if i < time_range / 5:
            x.append(5)
        elif i < 2*(time_range / 5):
            x.append(23)
        elif i < 3 * (time_range / 5):
            x.append(-8)
        elif i < 4 * (time_range / 5):
            x.append(14)
        else:
            x.append(-3)

    Ts = 1

    Kp = best_individual.chromosomes[0][0]
    Ti = best_individual.chromosomes[1][0]
    Td = best_individual.chromosomes[2][0]

    PidControlSystem = control_system.pid_control_system(x_list=x, Kp=Kp, Ti=Ti, Td=Td, Ts=Ts)
    ControlObject = control_system.ControlObject(0.1, 0.5)
    y = PidControlSystem.simulate_control_system(time_range=time_range, control_object=ControlObject)


    t = np.arange(0, time_range)

    lines = plt.plot(t, x, t, y)
    plt.setp(lines[0], linewidth=2)
    plt.setp(lines[1], linewidth=1)

    plt.legend(['expected output (y(t))', 'real output (r(t))'])
    plt.grid()
    plt.show()
    pass






