class ControlObject:
    def __init__(self, T, K):
        self.T = T
        self.K = K
        self.last_y = 0

    def generate_output(self, x):
        return self.T * self.last_y + self.K * x

class pid_control_system(ControlObject):
    def __init__(self, x_list, Kp, Ti, Td, Ts):
        self.x_list = x_list
        self.Kp = Kp
        self.Ti = Ti
        self.Td = Td
        self.Ts = Ts

    def simulate_control_system(self, time_range, control_object):
        object = control_object
        y_list = [0]
        e = [0, 0]
        u = [0]

        # PID
        b0 = self.Kp * (1 + self.Td / self.Ts)
        b1 = -self.Kp * (1 + 2 * self.Td / self.Ts - self.Ts / self.Ti)
        b2 = self.Kp * self.Td / self.Ts

        for i in range(1, time_range):
            e.append(self.x_list[i] - y_list[i - 1])
            u_ = u[i - 1] + b0 * e[i + 1] + b1 * e[i - 1 + 1] + b2 * e[i - 2 + 1]
            u.append(u_)
            y_object = object.generate_output(u[-1])
            y_list.append(y_object)

        return y_list

