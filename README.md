# Simple project of evolutionary algorithm.

Use python run.py:

You will see:

    Show the AG presentation? (y/n)
If You use 'y' - presentation of Genetic Algorithm, else - PID Control System Example.

### Presentation of Genetic Algorithm
I made some simple task:
```math
max(4-x^2-(y-1)^2)
```

Searching area:
```math
-25 <= x <= 25
```
```math
-25 <= y <= 25
```

The solution of this is:
```math
f(x=0,y=1) = 4 
```
If You start the program You will see 2 steps:
1. Working Genetic Algorithm:

        0 generation 
        1 generation 
        .
        .
        .
        200 generation
2. Creating the chart (Convergence to a solution):
![](img/gif.gif)



### PID Control System Example

Implemented simple PID Control System:

![](img/PID_en.svg.png)

[PID graph source](https://en.wikipedia.org/wiki/PID_controller)

The evolutionary algorithm looks for the optimal Kp, Ki, Kd values (PID controller settings).

I used this decision criteria (at this moment):

```math
\sum_{i=1}^{n}  |y[i] - r[i]|\quad
```

If You start the program You will see 2 steps:
1. Genetic algorithm is working:

        0 generation 
        1 generation 
        .
        .
        .
        200 generation
2. Chart  f(x) = y(x) - input and output of control system  (optimal K, Ki, Kd (PID controller settings)):

Used PID with optimal Kp, Ti, Td


![](img/chart_1.png)

And little bit worse solution:

![](img/chart_2.png)